// app.c

#include "hello.h"

int main(int argc, char const *argv[])
{
    hello("git");
    hello("github.com");
    hello("gitee.com");

    return 0;
}
